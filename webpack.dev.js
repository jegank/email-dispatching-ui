const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const urlManager = require('url-manager');
var urlRegex = /~\^\s*([a-zA-Z]+),\s*([^,^~]+),*\s*(.*?)\s*~\^/;
module.exports = merge(common, {
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './dist',
        hot: true,
        historyApiFallback: true,
        port: 3000
    },
    module: {
        rules: [{
            test: /\.(scss|css)$/,
            use: [{
                loader: "style-loader"
            }, {
                loader: "css-loader"
            }, {
                loader: "sass-loader"
            },

            ]
        },
        {
            test: /\.(jpg|png|gif|svg)$/,
            use: [
                {
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: './dist/assets/',
                    }
                }
            ]
        },
        {
            test: /\.(js|jsx|css)$/,
            use: [
                {
                    loader: 'string-replace-loader',
                    options: {
                        search: urlRegex.source,
                        replace: urlManager,
                        flags: 'g'
                    }
                }

            ]
        }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            title: 'ReactReduxSassStarterkit',
            inject: false,
            template: require('html-webpack-template'),
            bodyHtmlSnippet: '<main class="main" id="app"></main>'
        })
    ]
});
