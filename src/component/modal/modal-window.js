import React from 'react';
import '@trendmicro/react-modal/dist/react-modal.css';
import Modal from '@trendmicro/react-modal';
import { Button } from '@trendmicro/react-buttons';



export default () => {


    return (<Modal size={"sm"} onClose={false}>
        <Modal.Header>
            <Modal.Title>
                Modal Title
            </Modal.Title>
        </Modal.Header>
        <Modal.Body padding>
            Modal Body
        </Modal.Body>
        <Modal.Footer>
            <Button
                btnStyle="primary"
                onClick={false}
            >
                Save
            </Button>
            <Button
                btnStyle="default"
                onClick={false}
            >
                Close
            </Button>
        </Modal.Footer>
    </Modal>);
}