import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, change } from 'redux-form';
import { dispatchEmails, dispatchFile, sendServiceMail } from "../../redux/actions/upload-docs"
import Textbox from '../input-component/text-box';
import TextArea from '../input-component/text-area';
import UploadFile from '../input-component/upload-file';

class UpLoadDocs extends React.Component {

    constructor(props) {
        super(props)
        this.uploadAnyFile = this.uploadAnyFile.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.uploadEmailFile = this.uploadEmailFile.bind(this);
    }

    onFormSubmit() {
        let { sendService } = this.props;
        sendService();
    }

    uploadAnyFile(event) {
        let { pushFile } = this.props;
        if (window.File && window.FileReader && window.FileList && window.Blob && event.target.files) {
            pushFile(event.target.files[0]);
        }
    }

    render() {
        let { handleSubmit, loader, loadingVisibleItem, message } = this.props;
        return (<div>
            <section className="login_box_area section-margin">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-3">
                        </div>

                        <div className="col-lg-6">
                            <div className="login_form_inner">
                                <h3>EMAIL DISPATCHING TOOL</h3>

                                <span id="show-text"></span>

                                <form className="row login_form" onSubmit={handleSubmit(this.onFormSubmit)} action="#/"
                                    id="contactForm">

                                    <Field name="emailFile"
                                        component={UploadFile} label="Upload contact list with Notepad"
                                    />
                                    <Field name="uploadmailAttachment"
                                        component={UploadFile} label="Upload template(image or content)"
                                    />
                                    <Field name="subject" type="input"
                                        component={Textbox} label="Subject"
                                    />
                                    <Field name="mailContent"
                                        component={TextArea} label="Content"
                                    />
                                    <div className="col-md-12 form-group">
                                        <button type="submit" value="submit"
                                            className="button button-login w-100"> SUBMIT
                                    </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {message && <div>Mail was sent to given Receipients and file as well</div>}
        </div>);
    }
}
export const mapStateToProps = (state) => {
    let { emails, uploadFile, message } = state.uploadDocs;
    console.log("state----->", state);
    return {
        emails: emails,
        file: uploadFile,
        message: message
    }
}

export const mapDispatchToProps = (dispatch) => {
    return {
        pushEmails: (emails) => dispatch(dispatchEmails(emails)),
        pushFile: (file) => dispatch(dispatchFile(file)),
        sendService: () => dispatch(sendServiceMail())
    }
}
let fileUpload = reduxForm({
    form: 'UpLoadDocs',
    enableReinitialize: true
})(UpLoadDocs)
export default connect(mapStateToProps, mapDispatchToProps)(fileUpload);