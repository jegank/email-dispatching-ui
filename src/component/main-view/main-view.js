import React from 'react';
import Header from './header';
import Footer from './footer';
import "../../css/dashboard.css";
import "../../css/bootstrap.css";
import "../../css/index.css";
//import "../../css/style.css";


class MainView extends React.Component {
    render() {
        return (
            <div>
                <Header />
                <main className="site-main">
                    {this.props.children}
                </main>
                <Footer />
            </div>
        );
    }
};

export default (MainView);
