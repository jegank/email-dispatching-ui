import React from 'react';

const required = value => value ? undefined : 'Required'

export default ({ input, label, type, meta: { touched, error, warning } }) => (
    <div className="col-md-12 form-group">
        <label >{label}</label>
        <input {...input} className="form-control" type={type} />
        <div>{touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}</div>
    </div>
)

//export default class TextBox extends React.Component {
    // <label >{label}</label>
    // <input {...input} className="form-control" placeholder={label} type={type} />
    // <div>{touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}</div>
    //label
    //errormessage

    // render() {
    //     let { errorMessage, label, data } = this.props;
    //     return (<div className="form-group">
    //         <label for="exampleInputEmail1">CISCO Bug ID</label>
    //         <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
    //         <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
    //     </div>);
    // }

//}