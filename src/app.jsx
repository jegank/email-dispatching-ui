import React from 'react';
import { Route, Switch } from 'react-router-dom';
import MainView from './component/main-view/main-view';
import UpLoadDocs from './component/document-upload/upload-docs';

class App extends React.Component {

    render() {
        return (
            <MainView header={""} footer={""}>
                <Switch>
                    <Route exact path="/" component={UpLoadDocs} />
                </Switch>
            </MainView>
        );
    }
}

export default App;
