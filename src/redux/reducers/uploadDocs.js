import { service_call_success, service_call_failure, email_data, upload_file } from '../actions/upload-docs';
export default (state = [], action) => {
    switch (action.type) {
        case service_call_success:
            return {
                message: action.payLoad
            }
        case service_call_failure:
            return {
                error: action.payLoad
            }
        case email_data:
            return {
                ...state,
                emails: action.payLoad
            }
        case upload_file:
            return {
                ...state,
                uploadFile: action.payLoad
            }
        default:
            return state;
    }
};
