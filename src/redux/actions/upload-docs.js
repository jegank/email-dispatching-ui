import { serviceCallThunk } from "../service-call-thunk";
export const service_call_success = 'FILE_UPLOAD_SERVICE_CALL_SUCCESS';
export const service_call_failure = 'FILE_UPLOAD_SERVICE_CALL_FAILURE';
export const email_data = 'EMAIL_DATA';
export const upload_file = 'UPLOAD_FILE';



export const serviceCallSuccess = payLoad => {
    return {
        type: service_call_success,
        payLoad
    }
}

export const dispatchEmails = payLoad => {
    return {
        type: email_data,
        payLoad
    }
}

export const dispatchFile = payLoad => {
    return {
        type: upload_file,
        payLoad
    }
}

export const serviceCallFailure = payLoad => {
    return {
        type: service_call_failure,
        payLoad
    }
}



export const sendServiceMail = () => {

    return (dispatch, getState) => {
        let { subject, emailFile, uploadmailAttachment, mailContent } = getState().form.UpLoadDocs.values;
        let formData = new FormData();
        formData.append("attachmentFile", uploadmailAttachment);
        formData.append("mailReceipients", emailFile);
        formData.append("subject", subject);
        formData.append("mailContent", mailContent);
        const config = {
            url: `~^service, /sendingMail~^`,
            method: "POST",
            data: formData
        }
        return dispatch(serviceCallThunk(config, serviceCallSuccess, serviceCallFailure));
    }

}