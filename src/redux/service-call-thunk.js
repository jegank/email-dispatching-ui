import axios from "axios";

export const serviceCallThunk = (config, success, fail) => {

    return dispatch => new Promise((resolve, reject) => {
        axios(config).then((response) => {
            console.log("response--->", response);
            resolve(success ? dispatch(success(response.data)) : response.data);
        }).catch(error => {
            reject(fail ? dispatch(fail(error)) : error);
        });
    });
}
