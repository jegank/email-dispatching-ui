import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import uploadDocs from './reducers/uploadDocs';



export default combineReducers({
    form: formReducer,
    uploadDocs
});