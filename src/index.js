import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { store } from './redux/configure-store';
import { Route, BrowserRouter } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';

import App from './app.jsx';


ReactDOM.render((
    <Provider store={store}>
        <BrowserRouter>
            <Route path="/" component={App} />
        </BrowserRouter>
    </Provider>


), document.getElementById('app'));